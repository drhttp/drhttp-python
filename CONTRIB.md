## Build package :
```python
    python3 setup.py sdist bdist_wheel
```

## Upload to PyPI production servers:

*Note : pypi user is **drhttp***
```python
    python3 -m twine upload dist/*
```

## Upload to PyPI test servers :
```python
    python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*
```