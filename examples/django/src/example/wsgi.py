"""
WSGI config for example project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'example.settings')

application = get_wsgi_application()

if os.environ.get('USE_MOESIF_CLIENT', 'False') == 'False':
    import drhttp
    application = drhttp.WSGIMiddleware(app=application, dsn=os.environ['DRHTTP_DSN'])
