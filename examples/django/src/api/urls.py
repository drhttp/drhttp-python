from django.http import HttpResponseNotFound
from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('crash_test', views.crash, name='crash'),
    path('unexisting_path', lambda r: HttpResponseNotFound(), name='unexisting_path'),
    re_path('^.*$', views.echo, name='echo'),
]

