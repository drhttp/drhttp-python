from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

@require_http_methods(["GET", "POST"])
def index(request):
    return HttpResponse("Hello World!<br>This is an example. It has the role of your server of which you want to record io requests.")

@require_http_methods(["GET", "POST"])
def crash(request):
    return 1/0

@require_http_methods(["GET", "POST"])
def echo(request):
    return JsonResponse({
        "message": "This is an example server that echoes the incomming request",
        "logged_in_as": request.user.pk,
        "echoed": {
            "url": request.path,
            "headers": dict(request.headers),
            "body": request.body.decode('utf-8')
            }
    })