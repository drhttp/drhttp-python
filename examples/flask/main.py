import os
from flask import Flask, request, jsonify
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash


# Auth

auth = HTTPBasicAuth()
users = {u: generate_password_hash(u+u) for u in ['marc', 'marie', 'julien']}

@auth.verify_password
def verify_password(username, password):
    if username in users:
        return check_password_hash(users.get(username), password)
    return False

# App

app = Flask(__name__)

@app.route("/")
def hello():
    user = auth.username() or "World"
    return "Hello %s!<br>This is an example. It has the role of your server of which you want to record io requests." % user

@app.route("/crash_test", methods=['GET', 'POST'])
def crash():
    user = auth.username() or None
    return 1/0

@app.route("/unexisting_path", methods=['GET', 'POST'])
def nowhere():
    user = auth.username() or None
    return "glups", 404

@app.route("/<path:u_path>", methods=['GET', 'POST'])
def echo(u_path):
    return jsonify({
        "user": auth.username() or None,
        "echo data": request.get_data().decode('utf-8')
    })


if __name__ == '__main__':
    
    # Moesif
    if os.environ.get('USE_MOESIF_CLIENT', 'False') != 'False':
        from moesifwsgi import MoesifMiddleware
        try:
            from urllib.parse import urlparse
        except ImportError:
            from urlparse import urlparse
        dsn = urlparse(os.environ['DRHTTP_DSN'])
        app = MoesifMiddleware(app, {
            'APPLICATION_ID': dsn.username,
            'DEBUG': True,
            'LOCAL_MOESIF_BASEURL': dsn.scheme + '://' + dsn.hostname + '/moesif'
        })

    # DrHTTP
    else:
        # DrHTTP user identification example
        from drhttp import DRHTTP_HEADER_USER
        @app.after_request
        def identify_user(response):
            response.headers[DRHTTP_HEADER_USER] = auth.username()
            return response
        
        import drhttp   
        app = drhttp.WSGIMiddleware(app=app, dsn=os.environ['DRHTTP_DSN'])

    from werkzeug.serving import run_simple
    run_simple('0.0.0.0', 80, app,
               use_reloader=True, use_debugger=True, use_evalex=True,
               #extra_files=['/drhttp/client.py']
               )
