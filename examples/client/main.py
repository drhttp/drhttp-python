import json
import logging
import os
import time
import random
from reloadr import reloadr
import requests
import signal
from uuid import uuid4

logger = logging.getLogger('drhttp.client')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logging.getLogger('drhttp').setLevel(logging.INFO)

BASE_URL = 'http://api.my-server.tld/'
USERS = ['paul', 'marie', 'julien']
DELAY_BETWEEN_TWO_CALLS = 0.02

@reloadr
class Client:
    last_url_index = 0
    should_stop = False

    def __init__(self):
        signal.signal(signal.SIGTERM, self.stop)
        signal.signal(signal.SIGHUP, self.stop)
        signal.signal(signal.SIGINT, self.stop)

    def run(self):
        try:
            # Wait a random time (exit during this wait if asked)
            for _ in range(random.randint(1, 10)):
                if self.should_stop:
                    logger.info("Exiting gracefully...")
                    return False
                time.sleep(DELAY_BETWEEN_TWO_CALLS)
            
            # Generate & perform HTTP request
            method, url, user, json_body = self.generate_request()
            r = requests.request(method, url, json=json_body, auth=requests.auth.HTTPBasicAuth(user, user*2))
            logger.debug("Requested " + url)
            logger.debug("Response " + r.text)
            logger.info("[%.0fms] Retrieved %s" % (r.elapsed.total_seconds() * 1000, url))
            
        except requests.exceptions.ConnectionError as e:
            logger.error(e)
            
        return True

    def stop(self, *args):
        logger.debug("Asked to exit gracefully")
        self.should_stop = True

    def generate_request(self):
        # m, u = random.choices(population=[
        #     ('POST', 'user/' + str(random.randint(10000, 100000)) + '/migrate'),
        #     ('GET', 'user/' + str(random.randint(10000, 100000))),
        #     ('GET', 'purchases/order/' + str(random.randint(10000, 100000)) + '/verify'),
        #     ('GET', 'post/' + str(random.randint(10000, 100000)) + '/fact_check'),
        #     ('GET', 'post/' + str(random.randint(10000, 100000)) + '/translate'),
        #     ('GET', 'compliance/check'),
        #     ('GET', 'unexisting_path'),
        #     ('GET', 'crash_test'),
        # ], weights=[
        #     10, 10, 5, 3, 2, 4, 1, 1
        # ])[0]
        
        m, u = random.choices(population=[
            ('GET', 'user/' + str(random.randint(10000, 100000)) + '/groups'),
            ('POST', 'user/' + str(random.randint(10000, 100000))),
            ('GET', 'stores/order/' + str(random.randint(100000, 1000000))),
            ('GET', 'stores/order/' + str(random.randint(10000, 100000)) + '/cancel'),
            ('POST', 'device/register/' + str(uuid4())),
            ('DELETE', 'group/location/forget'),
        ], weights=[
            1, 5, 8, 1, 6, 2 
        ])[0]
        
        # m, u = random.choices(population=[
        #     ('GET', 'user/' + str(random.randint(10000, 100000)) + '/purchases/export'),
        #     ('GET', 'product/' + str(random.randint(10000, 100000)) + '/statistics'),
        #     ('GET', 'store/' + str(random.randint(100000, 1000000)) + '/sales'),
        #     ('GET', 'reporting/cities/'),
        # ], weights=[
        #     1, 5, 8, 1 
        # ])[0]
        
        # Generate user
        user = random.choice(USERS)

        # Generate body
        json_body = {
            "meta_info": "lorem ipsum",
            "my_object_collection": [{
                "id": i,
                "name": "name %d" % (i+1),
                "address": "21 jump street",
                "nb_stars": 5
            } for i in range(random.randint(1,10))]
        }
        
        return m, os.path.join(BASE_URL, u), user, json_body

Client._start_timer_reload(1)   # file watch does not work in docker

if __name__ == '__main__':
    a = True
    while a:
        a = Client().run()


