try:
    from urllib.parse import quote as quote
except ImportError:
    from urllib import pathname2url as quote
    
def extract_user_from_response_headers(headers, user_header_key):
    # Returns the tupe : user value, headers without user
    user = None
    for k, v in headers.items():
        if k.lower() == user_header_key.lower():
            user = v
            del headers[k]
            break
    return user, headers

def headers_from_wsgi_env(environ):
    # PEP 333 gives two headers which aren't prepended with HTTP_.
    HTTP_PREFIX = 'HTTP_'
    UNPREFIXED_HEADERS = {'CONTENT_TYPE', 'CONTENT_LENGTH'}
    
    headers = {}
    for k, v in environ.items():
        if k.startswith('HTTP_'):
            k = k[len('HTTP_'):]
        elif k not in UNPREFIXED_HEADERS:
            continue
        k = k.replace('_', '-').title()
        headers[k] = v
    return headers
    
def url_from_wsgi_env(environ):
    url = environ['wsgi.url_scheme'] + '://'

    if environ.get('HTTP_HOST'):
        url += environ['HTTP_HOST']
    else:
        url += environ['SERVER_NAME']

        if (environ['wsgi.url_scheme'] == 'https' and 
            environ['SERVER_PORT'] != '443') or \
            environ['SERVER_PORT'] != '80':
                url += ':' + environ['SERVER_PORT']
        
    url += quote(environ.get('SCRIPT_NAME', ''))
    url += quote(environ.get('PATH_INFO', ''))
    if environ.get('QUERY_STRING'):
        url += '?' + environ['QUERY_STRING']
    return url